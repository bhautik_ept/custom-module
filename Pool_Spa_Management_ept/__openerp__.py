{
    'name': 'Pool-Spa Management',
    'version': '1.0',
    'category': 'Pool-Spa Management',
    'description': """
This is extention of Pool-Spa Management
""",
    'author': 'Emipro Technologies Pvt. Ltd.',
    'website': 'www.emiprotechnologies.com',
    'depends': [
        'base_location_extension','calendar_service_ept'
    ],
    'data': [
        
    
        'xml/pool_surface.xml',
      #  'xml/enter_sanitising.xml',
      #  'xml/diff_sanitising_method.xml',
        'xml/site_service.xml',
       	'xml/pool_profile.xml',
        'xml/menu.xml',
        'xml/partner_site.xml',
        'xml/pos_site_res_partner.xml',
        'xml/res_partner_change.xml',
        'xml/product_template.xml',
        'xml/enter_sanitising_method.xml',
        
      
	    
        
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}
