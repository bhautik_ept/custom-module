from openerp import api, fields, models, _

class res_partner(models.Model):
    _inherit = 'res.partner'
    
    site_count = fields.Integer(compute='count_site')
    site_ids = fields.One2many('partner.site','customer_id',string='Site')
    
    @api.multi
    def count_site(self):
        for partner in self:
            partner.site_count = len(partner.site_ids)
    
    
    """
    Check the category context has key and category of value then search the category available in 
    res.partner.category
    """
    
    
    @api.multi    
    def name_get(self):
        
        categoryname = False
        category = self.env.context.has_key('category') and self.env.context['category'][0][2] or False
        #return super(res_partner,self).name_get()
        if category:
            categoryname = self.env['res.partner.category'].search([('id', '=', category[0])])
        
        if not categoryname:
            result = []
            for record in self:
                result.append((record.id,record.name))
            return result 
        
        else:
            result = []
            for record in self:
                result.append((record.id, record.name+categoryname.name))
            return result
        
    @api.multi
    def return_pool_profile(self):
        pool_profile_id = []
        site_id = self.env['partner.site'].search([('customer_id','=',self.id)])
        for site in site_id:
            #pool_profile_id = pool_profile_id + site.pool_site_ids.ids
            pool_profile_id += site.pool_site_ids.ids
            #pool_profile_id.append(pool_profile for pool_profile in site.pool_site_ids.ids)
        domain = [('id','in',pool_profile_id)]
        return {
            'name': 'Pool-Profile',
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'tree',
            'domain':domain,
            'res_model': 'pool.detail',
        }
        
        
        
        