from openerp import api, fields, models, _

class enter_sanitising_method(models.Model):
    _name = 'enter.sanitising.method'
    
    name = fields.Char(string='Sanitising Method')
    
    