from openerp import fields,models,api,_
from datetime import datetime, timedelta
from exceptions import Warning
from pygments.lexer import _inherit
from pkg_resources import require
from dateutil.relativedelta import relativedelta

class calendar_service_calendar(models.Model):
    _inherit = 'calendar.service.calendar'
    
    month = fields.Boolean('Every Month')
    
    
   
    
            
    
    @api.multi
    def name_get(self):
        result = []
        name = self._rec_name
        if name in self._fields:
            for record in self:
                rec_name = []
                if record.name:
                    rec_name.append(record.name)
                rec_name.append(self.get_weekday(record.weekday))
                rec_name.append("%s - %s" % (record.time_from, record.time_to))
                if record.rule_id.partner_ids:
                    for p_id in self.rule_id.partner_ids:
                        rec_name.append("/ %s" % (p_id.name))
                        result.append((record.id, ", ".join(rec_name)))    
        else:
            for record in self:
                result.append((record.id, "%s,%s" % (record._name, record.id)))
        return result
   
    