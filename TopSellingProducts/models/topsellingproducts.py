# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
 
from odoo import tools
from odoo import api, fields, models
 
 
class SaleReport(models.Model):
    _name = "top.selling.products.ept"
     
    _description = "Sales Orders Statistics"
    _auto = False
    _rec_name = 'date'
    _order = 'qty_delivered desc'

    date = fields.Datetime('Date Order', readonly=True)
    product_id = fields.Many2one('product.product', 'Product', readonly=True)
    qty_delivered = fields.Float('Quentity Delivered', readonly=True)
    categ_id = fields.Many2one('product.category', 'Product Category', readonly=True)

    
    def _select(self):
        select_str = """
            SELECT 
                min(sm.id) as id,
                sm.product_uom_qty  as qty_delivered ,
                sp.date_done as date ,
                pp.id as product_id,
                pt.categ_id as categ_id
           
        """ 
        return select_str
    def _from(self):
        from_str = """
                 
            stock_move sm 
            join stock_picking sp on (sm.origin = sp.origin) 
            join product_product pp on (sm.product_id = pp.id) 
                left join product_template pt on (pp.product_tmpl_id=pt.id)
    
                 
         """
        return from_str
    
    def _where(self):
        where_str = """
        
        sp.state = 'done'
            
        """
        return where_str
    
    def _group_by(self):
        group_by_str = """
            GROUP BY 
             
            sm.product_uom_qty,
            sp.date_done,
            pp.id,
            pt.categ_id 

            """
        return group_by_str
     
    def _order_by(self):
         
        order_by_str = """
         
            sm.product_uom_qty  
         
        """
        return order_by_str
  
    @api.model_cr
    def init(self):
        # self._table = sale_report
        tools.drop_view_if_exists(self.env.cr, self._table)
        self.env.cr.execute("""CREATE or REPLACE VIEW %s as (
            %s
            FROM ( %s ) WHERE %s
            %s ORDER  BY %s DESC
            )""" % (self._table, self._select(), self._from(),self._where(), self._group_by(),self._order_by()))
 
 
 
 

#===============================================================================
# 
# # -*- coding: utf-8 -*-
# # Part of Odoo. See LICENSE file for full copyright and licensing details.
# 
# from odoo import tools
# from odoo import api, fields, models
# 
# 
# class SaleReport(models.Model):
#     _name = "top.selling.products.ept"
#     
#     _description = "Sales Orders Statistics"
#     _auto = False
#     _rec_name = 'date'
#     _order = 'qty_delivered desc'
# 
#     #select sm.product_uom_qty   ,sp.date_done  , pt.name from stock_move sm join stock_picking sp on (sm.origin = sp.origin) join product_product pp on (sm.product_id = pp.id) join product_template pt on (pp.product_tmpl_id = pt.id) where sp.state = 'done'
# 
# 
# 
# 
#     date = fields.Datetime('Date Order', readonly=True)
#     product_id = fields.Many2one('product.product', 'Product', readonly=True)
#     qty_delivered = fields.Float('Quentity Delivered', readonly=True)
#     qty_to_invoice = fields.Float('Qty To Invoice', readonly=True)
#     qty_invoiced = fields.Float('Qty Invoiced', readonly=True)
#     partner_id = fields.Many2one('res.partner', 'Partner', readonly=True)
#     company_id = fields.Many2one('res.company', 'Company', readonly=True)
#     user_id = fields.Many2one('res.users', 'Salesperson', readonly=True)
#     price_total = fields.Float('Total', readonly=True)
#     price_subtotal = fields.Float('Untaxed Total', readonly=True)
#     product_tmpl_id = fields.Many2one('product.template', 'Product Template', readonly=True)
#     categ_id = fields.Many2one('product.category', 'Product Category', readonly=True)
#     nbr = fields.Integer('# of Lines', readonly=True)
#     pricelist_id = fields.Many2one('product.pricelist', 'Pricelist', readonly=True)
#     analytic_account_id = fields.Many2one('account.analytic.account', 'Analytic Account', readonly=True)
#     team_id = fields.Many2one('crm.team', 'Sales Team', readonly=True, oldname='section_id')
#     country_id = fields.Many2one('res.country', 'Partner Country', readonly=True)
#     commercial_partner_id = fields.Many2one('res.partner', 'Commercial Entity', readonly=True)
#     state = fields.Selection([
#         ('draft', 'Draft Quotation'),
#         ('sent', 'Quotation Sent'),
#         ('sale', 'Sales Order'),
#         ('done', 'Sales Done'),
#         ('cancel', 'Cancelled'),
#         ], string='Status', readonly=True)
#     weight = fields.Float('Gross Weight', readonly=True)
#     volume = fields.Float('Volume', readonly=True)
# 
#     def _select(self):
#         select_str = """
#              SELECT min(l.id) as id,
#                     l.product_id as product_id,
#                     t.uom_id as product_uom,
#                     sum(l.product_uom_qty / u.factor * u2.factor) as product_uom_qty,
#                     sum(l.qty_delivered / u.factor * u2.factor) as qty_delivered,
#                     sum(l.qty_invoiced / u.factor * u2.factor) as qty_invoiced,
#                     sum(l.qty_to_invoice / u.factor * u2.factor) as qty_to_invoice,
#                     null as price_total,
#                     null as price_subtotal,
#                     count(*) as nbr,
#                     s.name as name,
#                     s.date_order as date,
#                     s.state as state,
#                     s.partner_id as partner_id,
#                     s.user_id as user_id,
#                     s.company_id as company_id,
#                     extract(epoch from avg(date_trunc('day',s.date_order)-date_trunc('day',s.create_date)))/(24*60*60)::decimal(16,2) as delay,
#                     t.categ_id as categ_id,
#                     s.pricelist_id as pricelist_id,
#                     s.project_id as analytic_account_id,
#                     s.team_id as team_id,
#                     p.product_tmpl_id,
#                     partner.country_id as country_id,
#                     partner.commercial_partner_id as commercial_partner_id,
#                     sum(p.weight * l.product_uom_qty / u.factor * u2.factor) as weight,
#                     sum(p.volume * l.product_uom_qty / u.factor * u2.factor) as volume
#         """ 
#         return select_str
# 
#     def _from(self):
#         from_str = """
#                 sale_order_line l
#                       join sale_order s on (l.order_id=s.id)
#                       join res_partner partner on s.partner_id = partner.id
#                         left join product_product p on (l.product_id=p.id)
#                             left join product_template t on (p.product_tmpl_id=t.id)
#                     left join product_uom u on (u.id=l.product_uom)
#                     left join product_uom u2 on (u2.id=t.uom_id)
#                     left join product_pricelist pp on (s.pricelist_id = pp.id)
#         """
#         return from_str
# 
#     def _group_by(self):
#         group_by_str = """
#             GROUP BY l.product_id,
#                     l.order_id,
#                     t.uom_id,
#                     t.categ_id,
#                     s.name,
#                     s.date_order,
#                     s.partner_id,
#                     s.user_id,
#                     s.state,
#                     s.company_id,
#                     s.pricelist_id,
#                     s.project_id,
#                     s.team_id,
#                     p.product_tmpl_id,
#                     partner.country_id,
#                     partner.commercial_partner_id
#         """
#         return group_by_str
#     
#     def _order_by(self):
#         
#         order_by_str = """
#         
#             qty_delivered 
#         
#         """
#         return order_by_str
#  
#     @api.model_cr
#     def init(self):
#         # self._table = sale_report
#         tools.drop_view_if_exists(self.env.cr, self._table)
#         self.env.cr.execute("""CREATE or REPLACE VIEW %s as (
#             %s
#             FROM ( %s )
#             %s ORDER  BY %s DESC
#             )""" % (self._table, self._select(), self._from(), self._group_by(),self._order_by()))
# 
# 
# 
# 
# 
# 
# 
# 
# 
# 
# 
# 
# 
# 
#===============================================================================



#===============================================================================
# 
# 
# 
# 
#     
#     '''from odoo import models, fields, api, tools
#      
#      
#     class TopSaleProducts(models.Model):
#         
#         
#         _name = "top.selling.products"
#         
#         name = fields.Char('Order Reference', readonly=True)    
#         
#     '''    
#         
#     '''
#     
#     @api.model_cr
#     def init(self):
#        
#         tools.drop_view_if_exists(self.env.cr, self._table)
#         self.env.cr.execute("""CREATE or REPLACE VIEW top_selling_products as (
#          
#              select name as name from (sale_order_line) 
#          
#           )""" )
# '''
# 
# #===============================================================================
# # from odoo import models, fields, api, tools
# # 
# # 
# # class TopSaleProducts(models.Model):
# #     _name = "top.selling.products"
# #     
# #     product_id = fields.Many2one('product.product', 'Product', readonly=True)
# #     #date = fields.Datetime('Date Order', readonly=True)
# #     #qty_delivered = fields.Float('Qty Delivered', readonly=True)
# #     
# #     @api.model_cr
# #     def init(self):
# #         # self._table = sale_report
# #         tools.drop_view_if_exists(self.env.cr, self._table)
# #         self._cr.execute("""CREATE OR REPLACE VIEW top.selling.products AS 
# #         
# #             SELECT
# #                 
# #                 name as product_id
# #             FROM  sale_order_line
# #         
# #         """)
# #     
# #     
# #===============================================================================
#     
#     
# #===============================================================================
# #     
# #     def _select(self):
# #         select_str = """
# #             SELECT 
# #             name as product_id
# #         
# #         
# #         """
# #     
# #         return select_str
# #     
# #    
# #     def _from(self):
# #         from_str = """
# #     
# #         sale_order_line
# #     
# #         """
# #         return from_str
# #     
# #     @api.model_cr
# #     def init(self):
# #         # self._table = sale_report
# #         tools.drop_view_if_exists(self.env.cr, self._table)
# #         self.env.cr.execute("""CREATE or REPLACE VIEW  top_selling_products as (
# #             %s
# #             FROM ( %s )
# #            
# #             )""" % (self._select(), self._from()))
# # 
# #     
# #===============================================================================
#     
#     
#     
#     
# 
#     ''' 
#     def _select(self):
#         
#         select_str = """
#             SELECT 
#                 l.name as product_id
#                     
#         """
#         
#         return select_str
# 
# 
#     def _from(self):
#         from_str = """
#         
#             sale_order_line l 
#             
#         
#         
#         """
#         
#         return from_str
#   
#     #===========================================================================
#     # def _group_by(self):
#     #     group_by_str = """
#     #         GROUP BY product_template.name
#     #              
#     #     """
#     #     return group_by_str    
#     #===========================================================================
#   
#     
#    
#     @api.model_cr
#     def init(self):
#         
#         temp = 'SELECT name as product_id  FROM sale_order_line'
#         
#         tools.drop_view_if_exists(self.env.cr, self._table)
#         self.env.cr.execute("""CREATE or REPLACE VIEW top_selling_products as (
#           
#             SELECT name as product_id  FROM sale_order_line
#             
#             )""")
#         
#         
#          ''' 
# 
#     
# 
# '''
# from odoo import models, fields
# 
# 
# class TopSaleProducts(models.Model):
#     _name = "top.selling.products"
#     
# 
#     date_order = fields.Many2one('sale.order', readonly=True)
#     product_name = fields.Many2one('sale_order_line', readonly=True)
#     
#     
#     
#     
# 
#     state = fields.Selection([
#         ('draft', 'Draft Quotation'),
#         ('sent', 'Quotation Sent'),
#         ('sale', 'Sales Order'),
#         ('done', 'Sales Done'),
#         ('cancel', 'Cancelled'),
#         ], string='Status', readonly=True)
# 
# 
# 
# 
# 
# '''
# '''
# 
# # -*- coding: utf-8 -*-
# # Part of Odoo. See LICENSE file for full copyright and licensing details.
# 
# from odoo import api, fields, models, tools
# 
# 
# 
# class SaleReport(models.Model):
#     _name = "top.selling.products"
#     _order = 'product_id desc'
# 
#     name = fields.Many2one('sale.order', readonly=True)
#     date = fields.Many2one('sale.order', readonly=True)
#     product_id = fields.Many2one('sale_order_line', 'Product', readonly=True)
#     product_uom = fields.Many2one('product.template', 'Unit of Measure', readonly=True)
#     product_uom_qty = fields.Float('# of Qty', readonly=True)
#     qty_delivered = fields.Float('Qty Delivered', readonly=True)
# 
#     state = fields.Selection([
#         ('draft', 'Draft Quotation'),
#         ('sent', 'Quotation Sent'),
#         ('sale', 'Sales Order'),
#         ('done', 'Sales Done'),
#         ('cancel', 'Cancelled'),
#         ], string='Status', readonly=True)
#   
#     def _select(self):
#         select_str = """
#             
#              SELECT 
#                     s.name as name,
#                     s.date_order as date,
#                     l.product_id as product_id,
#                     t.uom_id as product_uom,
#                     sum(l.product_uom_qty / u.factor * u2.factor) as product_uom_qty,
#                     sum(l.qty_delivered / u.factor * u2.factor) as qty_delivered
#                     
#         """
#         return select_str
# 
#     def _from(self):
#         from_str = """
#                 sale_order_line l
#                       join sale_order s on (l.order_id=s.id)
#                       join res_partner partner on s.partner_id = partner.id
#                         left join product_product p on (l.product_id=p.id)
#                             left join product_template t on (p.product_tmpl_id=t.id)
#                     left join product_uom u on (u.id=l.product_uom)
#                     left join product_uom u2 on (u2.id=t.uom_id)
#                     left join product_pricelist pp on (s.pricelist_id = pp.id)
#                     
#         """
#         return from_str
# 
#     def _group_by(self):
#         group_by_str = """
#             GROUP BY s.name
#                    
#                     
#                     
#         """
#         return group_by_str
#      
#     @api.model_cr
#     def init(self):
#         
#         tools.drop_view_if_exists(self.env.cr, self._table)
#         self.env.cr.execute("""CREATE or REPLACE VIEW %s as (
#             %s
#             FROM ( %s )
#             %s
#             )""" % (self._table, self._select(), self._from(), self._group_by()))
# 
# 
# 
# '''
# 
# 
# '''
# from odoo import models, fields, api, tools
# 
# 
# class TopSaleProducts(models.Model):
#     _name = "top.selling.products"
#     
#     
#     
# 
#     product = fields.Many2one('product.template', 'Product', readonly=True)
#     date = fields.Datetime('Date Order', readonly=True)
#     qty = fields.Float('')
#     
#     
#     
#     state = fields.Selection([
#         ('draft', 'Draft Quotation'),
#         ('sent', 'Quotation Sent'),
#         ('sale', 'Sales Order'),
#         ('done', 'Sales Done'),
#         ('cancel', 'Cancelled'),
#         ], string='Status', readonly=True)
#     
#     
#     
#     
#  
#     @api.model_cr
#     def init(self):
#         
#         
#         qry="select  product_template.name as product , date as date, product_qty as Qty from stock_move inner join product_template on stock_move.product_id = product_template.id "
#         
#         tools.drop_view_if_exists(self.env.cr, self._table)
#         self.env.cr.execute("""CREATE or REPLACE VIEW %s  (
#             %s
#             
#            
#             )""" % (self._table, self._select(), self._from()))
#     
#     
#     
#     
#     
#     
#     def _select(self):
#         
#         select_str = """
#             SELECT product_template.name as product ,
#                     date as date,
#                     product_qty as qty
#                     
#         """
#         
#         return select_str
# 
# 
#     def _from(self):
#         from_str = """
#         
#             stock_move inner join product_template on stock_move.product_id = product_template.id 
#         
#         
#         """
#         
#         return from_str
#   
#     def _group_by(self):
#         group_by_str = """
#             GROUP BY product_template.name
#                  
#         """
#         return group_by_str    
#     
#     @api.model_cr
#     def init(self):
#         tools.drop_view_if_exists(self.env.cr, self._table)
#         self.env.cr.execute("""CREATE or REPLACE VIEW %s  (
#             %s
#             FROM ( %s )
#            
#             )""" % (self._table, self._select(), self._from()))
# 
#     
#       ''' 
#   
#===============================================================================
