{
    'name': 'Top selling products',
    'category': 'Sales',
    'sequence': 1,
    'summary': 'Top selling products based analysis report',
    'description': """
    
    
    """,
    
    'depends': ['base','sale','sales_team', 'account', 'procurement', 'report', 'web_tour'],
    'data': [          
        'views/sale_report.xml'
    ],

    'installable': True,
    'auto_install': False,
    'application': True,
}
