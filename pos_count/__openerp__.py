{
    'name': 'Point Of Sale',
    'version': '1.0',
    'category': 'Count_sale',
    'description': """
This is extention of Point Of Sale module
""",
    'author': 'Emipro Technologies Pvt. Ltd.',
    'website': 'www.emiprotechnologies.com',
    'depends': [
        'point_of_sale'
    ],
    'data': [
        'xml/add_pos_sale_kanban.xml',
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}